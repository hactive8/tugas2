let shape = new Shape(1, 20, 20),
    rectangle = new Rectangle(2, 30, 30, 90, 90),
    circle = new Circle(1, 40, 40, 10),
    eye = new Eye(1, 1, 2, 1, "red");

console.log(shape)
console.log(shape.move(10, 3))
console.log(rectangle)
console.log(circle)
console.log(eye)
console.log(eye)
console.log(eye.roll(3, 1))
// circle, rectangle, eye